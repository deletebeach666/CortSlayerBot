/* TelegramBot for NodeJS=
 * @author: Om Bhende
 * @date: 25 April 2015
 * @file: index.js
*/
"use strict";

// Require Modules
const Telegram = require('node-telegram-bot-api');
const express = require('express');
const http = require('http');
const os = require('os');
const bodyParser = require('body-parser');
const api = require('./assets/exports');
const config = require('./config/config');
const handler = require('./assets/handler');
const events = require('./assets/event');

// Initialize
var app = express();
var urlParser = bodyParser.urlencoded({ extended: false });
app.use(bodyParser.json());
var httpServer = http.createServer(app);
httpServer.listen(config.getConfig("settings", "web.port"));
const client = new Telegram(config.getConfig("settings", "token"), { polling: true });
var eventHandler = events.eventHandler;
var dev = "-1001255241883";

/* Setup Web Hook */
app.post('/webhook', urlParser, function (req, res) {
    res.set('Content-Type', 'text/json');
    if (req.header("x-gitlab-event") === "Push Hook") {
        var body = req.body;
        var repo = body.repository;
        var commits = body.commits[0];
        client.sendMessage(dev, "*Webhook Recieved*\n[" + commits.message + "](https://gitlab.com/deletebeach666/CortSlayerBot/commit/" + commits.id + ")\n*Author:* " + commits.author.name + "\n*Email Address:* " + commits.author.email + "\n\n" + commits.timestamp, { "parse_mode": "markdown" })
    }
    res.send(JSON.stringify({ "success": true }));
});

/* Emit Event on Message */
client.on('message', (msg) => {
    eventHandler.emit("onMessage", client, msg);
});

// Sample onText which emits "onCommandHandler" event
client.onText(/\/(.*)/, (msg) => {
    var arr = msg.text.split(" ");
    var command = arr[0];
    arr[0] = null;
    arr = arr.filter(function () { return true; });
    api.clearObject(arr);
    eventHandler.emit("onCommandHandler", client, msg, command, arr);
});

/* Client Commands */
client.onText(/\/start/, (msg) => {
    client.sendMessage(msg.chat.id, "Hey There! Ask me something!", {
        "reply_markup": {
            "keyboard": [["What can you do?", "Who are you?"], ["Why are you so pro?", "Who made you?"]]
        }
    });
});

client.onText(/\/os/, (msg) => {
    var serverUptime = api.getTime(os.uptime());
    client.sendMessage(msg.chat.id, "*Platform*: " + os.platform() + "\n*Arch*: " + os.arch() + " \n*System Load*: " + (os.loadavg()[1] * 10).toFixed(2) + "% \n*Hostname*: " + os.hostname() + " \n*Uptime*: " + serverUptime, { "parse_mode": "markdown" });
});

client.onText(/\/repo/, (msg) => {
    client.sendMessage(msg.chat.id, "You can find my git repository on [GitLab](https://gitlab.com/deletebeach666/CortSlayerBot)! Make sure you contact @at0mic01 to give you access.", { "parse_mode": "markdown" });
});