﻿/* TelegramBot for NodeJS=
 * @author: Om Bhende
 * @date: 25 April 2015
 * @file: assets/handler.js
*/
"use strict";

/* Include all required modules */
const events = require('./event');
const api = require('./exports');
const handler = require('./handler');
const config = require('../config/config');

/* Initialize Event System */
const eventHandler = events.eventHandler;

/* Handle our first event: onMessage */
eventHandler.on('onMessage', function (client, data) {
    var name = data.from.first_name + " " + data.from.last_name;
    var type = data.chat.type;
    console.log("(" + type + " - " + data.chat.id + ") " + name + " (" + data.from.id + "): " + data.text);
    if (config.check4DefaultReply(data.text)) {
        client.sendMessage(data.chat.id, config.check4DefaultReply(data.text));
    }
});