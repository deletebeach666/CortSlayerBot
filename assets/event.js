﻿/* TelegramBot for NodeJS=
 * @author: Om Bhende
 * @date: 25 April 2015
 * @file: assets/event.js
*/
"use strict";


var events = require('events');
var em = new events.EventEmitter();
module.exports.eventHandler = em;