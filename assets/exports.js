﻿/* TelegramBot for NodeJS=
 * @author: Om Bhende
 * @date: 25 April 2015
 * @file: assets/exports.js
*/
"use strict";
/* Include all required modules */
const events = require('./event');

/* Initialize Event System */
const eventHandler = events.eventHandler;


exports.getTime = function getTime(seconds) {
    if (seconds) {
        var seconds = parseInt(seconds, 10);

        var days = Math.floor(seconds / (3600 * 24));
        seconds -= days * 3600 * 24;
        var hrs = Math.floor(seconds / 3600);
        seconds -= hrs * 3600;
        var mnts = Math.floor(seconds / 60);
        seconds -= mnts * 60;
        return days + " days, " + hrs + " hrs, " + mnts + " minutes, " + seconds + " seconds";
    } 
    return "-";
}

exports.clearObject = function clearObject(actual) {
    var newArray = new Array();
    for (var i = 0; i < actual.length; i++) {
        if (actual[i]) {
            newArray.push(actual[i]);
        }
    }
    return newArray;
}