﻿# Cort-Telegram-Bot
### Installation
- Clone/Download the repository
> git clone https://github.com/deletebeach666/Cort-Telegram-Bot.git
- First, get all dependencies
> npm install
- Then you need to copy the example config file for use
> cp config/config.json.example config/config.json
- Now edit the config file and run the bot with `node index.js`
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
### Support
- For any support you can contact me on discord.
> Om#6969

> [Discord Server](https://discordapp.com/invite/DqFUxFZ)


### Exported API/Events
> [Exported API/Events](https://docs.google.com/spreadsheets/d/1_DYf-s83D4-80kpXW2DnG5enWU3ywU2a5IWZ680BXLM)


