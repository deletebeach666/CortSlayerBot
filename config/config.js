﻿/* TelegramBot for NodeJS=
 * @author: Om Bhende
 * @date: 25 April 2015
 * @file: config/config.js
*/
"use strict";
const config = require('./config.json');
const defaultReplies = require('./default.replies.json');

exports.getConfig = function getConfig(type, key) {
    if (type && key) {
        return config[type][key];
    }
}

exports.check4DefaultReply = function check4DefaultReply(message) {
    if (message) {
        return defaultReplies[message]
    } else {
        return false;
    }
}
